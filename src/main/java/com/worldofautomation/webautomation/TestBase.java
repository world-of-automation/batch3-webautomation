package com.worldofautomation.webautomation;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestBase {

    private static WebDriver driver = null;
    private static ExtentReports extent;

    @Parameters({"url", "browserName", "cloud", "os"})
    @BeforeMethod
    public static void setupConfig(String url, String browserName, boolean cloud, String os) {
        if (cloud == true) {
            String bsUserName = "azharkautomation1";
            String bsAccessKey = "mFTz1sSxLoaBGPBJpFPQ";
            String urlForBS = "https://" + bsUserName + ":" + bsAccessKey + "@hub-cloud.browserstack.com/wd/hub";
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "Chrome");
            caps.setCapability("browser_version", "80.0");
            caps.setCapability("os", "OS X");
            caps.setCapability("os_version", "Catalina");
            caps.setCapability("resolution", "1024x768");
            caps.setCapability("name", "Bstack-[Java] Sample Test");

            URL urll = null;
            try {
                urll = new URL(urlForBS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            driver = new RemoteWebDriver(urll, caps);
        } else {
            if (os.equalsIgnoreCase("mac")) {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
                    driver = new FirefoxDriver();
                }
            } else {
                if (browserName.equalsIgnoreCase("chrome")) {
                    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                    driver = new ChromeDriver();
                } else if (browserName.equalsIgnoreCase("mozilla")) {
                    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                    driver = new FirefoxDriver();
                }
            }
        }
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
    }

    @AfterMethod
    public static void close() {
        driver.quit();
    }

    //*********************EXTENT STARTED*********************

    public static void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static void clickByXpath(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    public static void clickById(String id) {
        driver.findElement(By.id(id)).click();
    }

    //*********************EXTENT FINISHED*********************

    public static void clickByLinkText(String linkText) {
        driver.findElement(By.linkText(linkText)).click();
    }

    public static boolean validateXpathIsDisplayed(String xpath) {
        boolean ifDisplayed = driver.findElement(By.xpath(xpath)).isDisplayed();
        return ifDisplayed;
    }

    public static boolean validateListOfXpathIsDisplayed(String xpath) {
        List<WebElement> elementList = driver.findElements(By.xpath(xpath));
        boolean flag = false;

        for (int i = 0; i < elementList.size(); i++) {
            if (elementList.get(i).isDisplayed()) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static void sendKeysUsingId(String id, String keysToSend) {
        driver.findElement(By.id(id)).sendKeys(keysToSend);
    }

    public static void switchToTab(int tabNo) {
        ArrayList<String> tabList = new ArrayList<String>(getDriver().getWindowHandles());
        getDriver().switchTo().window(tabList.get(tabNo));
    }

    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each test cases / methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each test cases /methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report & Connection closed
    @AfterSuite(alwaysRun = true)
    public void closeReportAndConnections() {
        ConnectSQL.closeConnection();
        extent.close();
    }

    public void validateIfTextIsAsExpected(String xpath, String expectedText) {
        WebElement element = driver.findElement(By.xpath(xpath));
        String actualText = element.getText();
        Assert.assertEquals(actualText, expectedText);
    }

    public String getTextFromXpath(String xpath) {
        String text = driver.findElement(By.xpath(xpath)).getText();
        return text;
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    //WebDriverWait webDriverWait = new WebDriverWait(getDriver(), 20);
    //webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));

}
