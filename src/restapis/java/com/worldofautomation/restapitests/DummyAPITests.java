package com.worldofautomation.restapitests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class DummyAPITests {

    // http://dummy.restapiexample.com/


    @Test
    public void getAllTheEmployees() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().log().all().get("/employees")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        // get the data from db
        // validate the both data
    }

    @Test
    public void getSingleEmployee() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().log().all().get("/employee/1")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        // get the data from db
        // validate data is on the db
    }

    @Test
    public void deleteSingleEmployee() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        Response response = RestAssured.given().when().log().all().delete("/delete/84")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        // get the data from db
        // validate the data is not on db
    }


    @Test
    public void createAUser() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Zan");
        jsonObject.put("salary", "111111");
        jsonObject.put("age", "18");


        Response response = RestAssured.given().when().header("Content-Type", "application/json").body(jsonObject.toString()).log().all().post("/create")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());

        // get the data from db
        // check the both data

    }

    @Test
    public void updateAUser() {
        RestAssured.baseURI = "http://dummy.restapiexample.com/api/v1";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Zan");
        jsonObject.put("salary", "22222");
        jsonObject.put("age", "18");


        Response response = RestAssured.given().when().header("Content-Type", "application/json").body(jsonObject.toString()).log().all().put("/update/83")
                .then().assertThat().statusCode(200).extract().response();

        System.out.println(response.body().prettyPrint());


        // get the data from db
        // check the both data
    }

}
