package com.worldofautomation.models;

import lombok.Data;

@Data
public class Employee {
    private int id;
    private String name;
    private boolean permanent;
    private int phone_number;
    private String role;
    private String city;

    public Employee(int id, String name, boolean permanent, int phone_number, String role, String city) {
        this.id = id;
        this.name = name;
        this.permanent = permanent;
        this.phone_number = phone_number;
        this.role = role;
        this.city = city;
    }

    public Employee() {

    }
}
