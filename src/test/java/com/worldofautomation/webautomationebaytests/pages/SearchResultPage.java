package com.worldofautomation.webautomationebaytests.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SearchResultPage {

    @FindBy(xpath = "//h1[@class='srp-controls__count-heading']")
    private WebElement resultForValidation;

    public void validateUserIsOnSearchResultPage(String data) {
        String result = resultForValidation.getText();
        Assert.assertTrue(result.contains("results for " + data));
        ExtentTestManager.log("results for " + data + " has been displayed");
    }
}
