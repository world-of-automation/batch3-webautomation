package com.worldofautomation.webautomationebaytests.tests;

import com.worldofautomation.webautomation.ConnectSQL;
import com.worldofautomation.webautomation.ExtentTestManager;
import com.worldofautomation.webautomation.TestBase;
import com.worldofautomation.webautomationebaytests.DataGenerator;
import com.worldofautomation.webautomationebaytests.Queries;
import com.worldofautomation.webautomationebaytests.pages.HomePage;
import com.worldofautomation.webautomationebaytests.pages.RegisterPage;
import com.worldofautomation.webautomationebaytests.pages.SearchResultPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class HomePageFunctionality extends TestBase {
    private HomePage homePage;
    private RegisterPage registerPage;
    private SearchResultPage searchResultPage;

    @BeforeMethod
    private void setupInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
        registerPage = PageFactory.initElements(getDriver(), RegisterPage.class);
        searchResultPage = PageFactory.initElements(getDriver(), SearchResultPage.class);
    }

    @Test(enabled = false)
    public void validateUserIsAbleToLoginUsingValidCreds() {
        homePage.validateUserIsOnHomePage();
        homePage.clickOnRegisterOnButton();
        ExtentTestManager.log("clicked on register button");
        registerPage.fillFnameAndLname();
        ExtentTestManager.log("filled up first name and last name");
    }


    @Test(dataProviderClass = DataGenerator.class, dataProvider = "getSearchData", enabled = false)
    public void userBeingAbleToSearchForAnyItemOfTheirPreferrence(String data) {
        homePage.validateUserIsOnHomePage();
        homePage.typeOnSearchBar(data);
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(data);
    }


    @Test(enabled = false)
    public void validateUserIsAbleToSeeSearchGhostText() {
        homePage.validateUserIsOnHomePage();
        homePage.validateGhostTextInSearBox();
    }

    @Test
    public void userBeingAbleToSearchForAnyItemOfTheirPreferrenceDB() throws SQLException {
        Connection connection = ConnectSQL.getConnection("worldOfAutomation");
        Statement statement = ConnectSQL.getStatement(connection);
        ResultSet resultSet = ConnectSQL.getResultSet(statement, Queries.SELECT_QUERY);

        ArrayList<String> booksList = new ArrayList<String>();
        while (resultSet.next()) {
            booksList.add(resultSet.getString("Books"));
        }


        System.out.println(booksList);
        homePage.validateUserIsOnHomePage();
        homePage.typeOnSearchBar(booksList.get(0));
        homePage.clickOnSearchButton();
        searchResultPage.validateUserIsOnSearchResultPage(booksList.get(0));
    }

}

