package com.worldofautomation.webautomationebaytests.tests;

import com.worldofautomation.webautomation.TestBase;
import com.worldofautomation.webautomationebaytests.pages.HomePage;
import com.worldofautomation.webautomationebaytests.pages.RegisterPage;
import org.openqa.selenium.support.PageFactory;

public class EventCase extends TestBase {

    public void functionEventCase(functionNames functionName){

        HomePage homePage = PageFactory.initElements(getDriver(), HomePage.class);
        RegisterPage registerPage = PageFactory.initElements(getDriver(),RegisterPage.class);
        switch (functionName){

            //functinoNames.valueOf(funstionName)
          case FILL_FNAME_AND_LNAME:
              registerPage.fillFnameAndLname();
              break;
          case CLICK_ON_REGISTER_ON_BUTTON:
              homePage.clickOnRegisterOnButton();
              break;
          case VALIDATE_USER_IS_ON_HOME_PAGE:
              homePage.validateUserIsOnHomePage();
              break;
      }

    }

    public enum functionNames{
        VALIDATE_USER_IS_ON_HOME_PAGE, CLICK_ON_REGISTER_ON_BUTTON, FILL_FNAME_AND_LNAME
    }
}
