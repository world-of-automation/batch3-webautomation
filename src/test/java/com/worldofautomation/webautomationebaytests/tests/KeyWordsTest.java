package com.worldofautomation.webautomationebaytests.tests;
import org.testng.annotations.Test;

import static com.worldofautomation.webautomationebaytests.tests.EventCase.functionNames.*;

public class KeyWordsTest {

    @Test
    public void testMyKeyWordDrivenTestsing(){
        EventCase eventCase = new EventCase();
        eventCase.functionEventCase(VALIDATE_USER_IS_ON_HOME_PAGE);
        eventCase.functionEventCase(CLICK_ON_REGISTER_ON_BUTTON);
        eventCase.functionEventCase(FILL_FNAME_AND_LNAME);
    }

}
