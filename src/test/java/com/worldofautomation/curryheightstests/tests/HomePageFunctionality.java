package com.worldofautomation.curryheightstests.tests;

import com.worldofautomation.curryheightstests.pages.HomePage;
import com.worldofautomation.webautomation.TestBase;
import com.worldofautomation.webautomationfacebooktests.DataGenerator;
import com.worldofautomation.webautomationfacebooktests.tests.TabHandlerSelenium;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageFunctionality extends TestBase {

    HomePage homePage = null;

    @BeforeMethod
    public void setupInstances() {
        homePage = PageFactory.initElements(getDriver(), HomePage.class);
    }


    @Test(dataProviderClass = DataGenerator.class, dataProvider = "getCredentials2")
    public void userShouldnotBeAbleToLoginWithInvalidCreds(String username, String password) {
        homePage.validateFaceBookIsDisplayed();
        homePage.sendUsernameAndPassInRqdFields(username, password);
        homePage.clickOnLoginBtn();
        Logger logger = Logger.getLogger(TabHandlerSelenium.class);
        logger.info("homepage functionality has been validated ");
    }
}
