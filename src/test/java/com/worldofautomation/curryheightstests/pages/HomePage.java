package com.worldofautomation.curryheightstests.pages;

import com.worldofautomation.webautomation.ExtentTestManager;
import com.worldofautomation.webautomation.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {


    @FindBy(id = "email")
    private WebElement usernameField;

    @FindBy(id = "pass")
    private WebElement passField;

    @FindBy(id = "u_0_b")
    private WebElement loginBtn;

    public void validateFaceBookIsDisplayed() {
        String actualUrl = TestBase.getDriver().getCurrentUrl();
        String expectedUrl = "https://www.facebook.com/";
        Assert.assertEquals(actualUrl, expectedUrl);
        ExtentTestManager.log(actualUrl + " is validated and loaded");
    }

    public void sendUsernameAndPassInRqdFields(String username, String password) {
        usernameField.sendKeys(username);
        passField.sendKeys(password);
    }

    public void clickOnLoginBtn() {
        loginBtn.click();
    }
}