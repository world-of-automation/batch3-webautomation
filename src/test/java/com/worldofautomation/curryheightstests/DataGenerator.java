package com.worldofautomation.webautomationfacebooktests;

import com.worldofautomation.webautomation.Utilities;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;

public class DataGenerator {

    @DataProvider(name = "getCredentials")
    public Object[][] getCredentials() {
        JSONArray jsonArray = Utilities.getJSONArray("src/test/resources/TestData.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        String username = (String) jsonObject.get("username");
        String password = (String) jsonObject.get("password");
        Object[][] objects = new Object[1][2];
        objects[0][0] = username;
        objects[0][1] = password;
        return objects;
    }


    @DataProvider(name = "getCredentials2")
    public Object[][] getCredentials2() {
        return new Object[][]
                {{"user001", "pass327893"}, {"user002", "pass2342"}, {"user003", "pass2252"}};
    }
}
